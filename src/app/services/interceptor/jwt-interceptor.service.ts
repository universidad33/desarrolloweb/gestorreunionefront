import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpInterceptor
} from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from 'app/services/token.service';
import { environment } from 'environments/environment';
import { AlertService } from 'app/services/utils/alert.service';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  constructor(private router: Router, private tokenService: TokenService, 
    private alertService: AlertService) { }
    
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
  {
    const token = this.tokenService.getToken();
    const isLogin = (request.url == environment.urlApi + '/auth/login') ? true : false;
    if(!isLogin) {
      if (token) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`
          }
        });
      }
    }

    return next.handle(request).pipe(
        tap(event => {
            if (event instanceof HttpResponse) {
                // http response status code
                if(typeof event.body.status != "undefined" && event.body.status == "999") {
                  this.tokenService.clearToken();
                  this.router.navigate(['/login']);
                }

                if(typeof event.body.status != "undefined" && event.body.status == "423") {
                  this.alertService.warning('Validacion', 'Se detecto el uso indebido de scripts, sql o palabras del sistema, valide la informacion que intenta cargar');
                }
            }
        })
    )
    
  }
}
