import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

export interface AuthMe {
  status: number;
  data : any;
}

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private _router: Router,private http: HttpClient) { }

  userLoggin() {
    const token = localStorage.getItem('resourceToken');
    if (token === undefined || token === null) {
      this._router.navigate(['/login']);
      return false;
    } else {
      this._router.navigate(['/home']);
    }
    return true;
  }

  setToken(token) {
    localStorage.setItem('resourceToken', token);

    if (localStorage.getItem('resourceToken') !== null) {
      return true;
    } else {
      return false;
    }
  }

  getDataUserFromToken(param): Observable<AuthMe> {
    return this.http.post<AuthMe>( environment.urlApi + '/auth/me', param).pipe();
  }

  getToken(){
      return localStorage.getItem('resourceToken');
  }
  
  clearToken(){
    localStorage.clear();
  }
}
