import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DataUser } from 'app/commons/dataUser'; 

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( private http: HttpClient ) { }

  getUserAll(){
    return this.http.get<any>(environment.urlApi +'/user/list', {});
  }

  getRoles(){
    return this.http.get<any>(environment.urlApi +'/user/roles', {});
  }

  getUser(){
    return DataUser.user;
  }

  setUser(user:any){
    DataUser.user = user;
  }

  addUser(data:any) {
    return this.http.post<any>(environment.urlApi + '/user/register', data);
  }

}
