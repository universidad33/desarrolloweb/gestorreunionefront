import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class CrytAESService {

  constructor() { /** */ }

  encrypt(text:any) {
    var salt = CryptoJS.lib.WordArray.random(256);
    var iv = CryptoJS.lib.WordArray.random(16);

    var key = CryptoJS.PBKDF2(environment.keyCrypt, salt, { hasher: CryptoJS.algo.SHA512, keySize: 64/8, iterations: 999 });

    var encrypted = CryptoJS.AES.encrypt(text, key, {iv: iv});

    var data = {
        ciphertext : CryptoJS.enc.Base64.stringify(encrypted.ciphertext),
        salt : CryptoJS.enc.Hex.stringify(salt),
        iv : CryptoJS.enc.Hex.stringify(iv)    
    }

    return window.btoa(JSON.stringify(data));
  }

  decrypt(jsonstring:any) {
    try {
      var obj_json = JSON.parse(window.atob(jsonstring));

      var encrypted = obj_json.ciphertext;
      var salt = CryptoJS.enc.Hex.parse(obj_json.salt);
      var iv = CryptoJS.enc.Hex.parse(obj_json.iv);   
  
      var key = CryptoJS.PBKDF2(environment.keyCrypt, salt, { hasher: CryptoJS.algo.SHA512, keySize: 64/8, iterations: 999});
      var decrypted = CryptoJS.AES.decrypt(encrypted, key, { iv: iv});
  
      return decrypted.toString(CryptoJS.enc.Utf8);
    } catch (error) {  
      return '';
    }
   
  }

}