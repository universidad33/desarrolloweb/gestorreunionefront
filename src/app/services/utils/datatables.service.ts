import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AlertService } from 'app/services/utils/alert.service';
class DataTablesResponse {
  status: any;
  msm: any;
  data: {
    delected: any;
    id: any;
    name: any;
    id_user_edit: any;
  }
}



@Injectable({
  providedIn: 'root'
})
export class DatatablesService {

  data:any;
  dtOptions: DataTables.Settings = {};
  constructor(private http: HttpClient, private alertService: AlertService) { }

  config(pageLengths = 10){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: pageLengths,
      processing: true,
      autoWidth: true,
      responsive: true,
      language: {
        processing: `<div class="d-flex flex-column align-items-center justify-content-center">
          <div class="card col-3">
            <div class="card-body d-flex flex-column align-items-center justify-content-center">
              <div class="row">
                  <div class="spinner-border text-primary" role="status">
                      <span class="sr-only">Loading...</span>
                  </div>
               </div>
               <div class="row">
                 <strong>Procesando...</strong>
               </div>
            </div>
          </div>
        </div>`,
        search: "Buscar:",
        lengthMenu: "",
        info: "Mostrando desde _START_ al _END_ de _TOTAL_ elementos",
        infoEmpty: "Mostrando ningún elemento.",
        infoFiltered: "(filtrado _MAX_ elementos total)",
        infoPostFix: "",
        loadingRecords: "Cargando registros...",
        zeroRecords: "No se encontraron registros",
        emptyTable: "No hay datos disponibles en la tabla",
        paginate: {
          first: "Primero",
          previous: "Anterior",
          next: "Siguiente",
          last: "Último"
        },
        aria: {
          sortAscending: ": Activar para ordenar la tabla en orden ascendente",
          sortDescending: ": Activar para ordenar la tabla en orden descendente"
        }
      },
    };

    return this.dtOptions;
  }

  configServerSide(url:any, columsTable:any, pageLengths = 10){
    const that = this;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: pageLengths,
      serverSide: true,
      processing: true,
      autoWidth: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http.post<DataTablesResponse>(environment.urlApi + url, dataTablesParameters, {})
          .subscribe(resp => {
            if(resp.status == 200){
              callback({
                recordsTotal: resp.data.name,
                recordsFiltered: resp.data.name,
                data: resp.data
              });
            } else {
              if(resp.status == 422){
                var msgerror = '';
                // $.each(resp.errors, function (indexInArray, valueOfElement) {
                //     msgerror += valueOfElement + '<br/>';
                // });
                this.alertService.warning('Validacion', msgerror);
              } 

              callback({
                recordsTotal: null,
                recordsFiltered: null,
                data: null
              });
            }
          });
      },
      responsive: true,
      language: {
        processing: `<div class="d-flex flex-column align-items-center justify-content-center">
          <div class="card col-3">
            <div class="card-body d-flex flex-column align-items-center justify-content-center">
              <div class="row">
                  <div class="spinner-border text-primary" role="status">
                      <span class="sr-only">Loading...</span>
                  </div>
               </div>
               <div class="row">
                 <strong>Procesando...</strong>
               </div>
            </div>
          </div>
        </div>`,
        search: "Buscar:",
        lengthMenu: "",
        info: "Mostrando desde _START_ al _END_ de _TOTAL_ elementos",
        infoEmpty: "Mostrando ningún elemento.",
        infoFiltered: "(filtrado _MAX_ elementos total)",
        infoPostFix: "",
        loadingRecords: "Cargando registros...",
        zeroRecords: "No se encontraron registros",
        emptyTable: "No hay datos disponibles en la tabla",
        paginate: {
          first: "Primero",
          previous: "Anterior",
          next: "Siguiente",
          last: "Último"
        },
        aria: {
          sortAscending: ": Activar para ordenar la tabla en orden ascendente",
          sortDescending: ": Activar para ordenar la tabla en orden descendente"
        }
      },
      columns: columsTable,
    };

    return this.dtOptions;
  }
}
