import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MeetService {

  constructor( private http: HttpClient ) { }
  
  getAllMeet(){
    return this.http.get<any>(environment.urlApi + '/meet/getAllMeet', {});
  }

  addMeet(data) {
    return this.http.post<any>(environment.urlApi + '/meet/addMeet', data);
  }

}
