import { Component} from '@angular/core';
import { TokenService } from './services/token.service';
import { Router, Event, NavigationStart} from '@angular/router';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';;
import { Keepalive } from '@ng-idle/keepalive';
import { UsersService } from 'app/services/users.service';
import { LoginService } from 'app/services/login.service';
import { AlertService } from 'app/services/utils/alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  permission: boolean = true;
  loading: boolean = false;
  lastPing?: Date = null;
  token:any;

  constructor(private router: Router, 
    private tokenService: TokenService, 
    private usersService: UsersService,
    private alertService: AlertService,
    private loginService: LoginService,
    private idle: Idle, 
    private keepalive: Keepalive) {

      this.token = this.tokenService.getToken();
      this.idle.setIdle(600);
      this.idle.setTimeout(5);
      this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
      
      this.keepalive.interval(15);
      this.keepalive.onPing.subscribe(() => { 
        this.lastPing = new Date(); 
      });
  
      this.idle.onTimeout.subscribe(() => {
        this.alertService.warning('Sesion expirada', 'La sesión ha expirado, sera redirigido al inicio de sesión.');
        this.logOut();
      });
      
      if (this.token) {
        this.idle.watch();
      } else {
        this.idle.stop();
      }

      this.router.events.subscribe((event: Event) => {
        this.permissions(event);
      });
      
  }

  permissions(event: Event){

      if (event instanceof NavigationStart) {
        (this.token) ? this.idle.watch() : this.idle.stop() ;
        if(event.url != '/login'){
          this.permission = false;

          this.getDataUserToken(event.url);
        }else{
          this.permission = true;
          if(this.token){
            this.router.navigate(['/login']);
          }
        }
      }
  }

  getDataUserToken(url:any){
    var data = new FormData();
    data.append('path', url);

    this.tokenService.getDataUserFromToken(data).subscribe(resp => {
      if(resp.status === 200){
          this.permission = true;
           
        this.usersService.setUser(resp.data);
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  logOut() {
    this.loginService.logout().subscribe(
      (response: any) => {
        localStorage.clear();
        location.reload();
      });
  }
}
