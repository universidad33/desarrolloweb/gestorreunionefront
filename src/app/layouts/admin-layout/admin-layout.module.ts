import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { UsersComponent } from '../../pages/users/users.component';
import { HomeComponent } from '../../pages/home/home.component';
import { MeetComponent } from '../../pages/monitoring/meet.component';
import { CreateEditUsersComponent } from '../../pages/users/modal/create-edit-users/create-edit-users.component';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { ComponentsModule } from 'app/components/components.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    DataTablesModule,
    MatTooltipModule,
    ComponentsModule,
    MatDatepickerModule,
  ],
  declarations: [
    HomeComponent,
    UsersComponent,
    MeetComponent,
    CreateEditUsersComponent,
  ],
  entryComponents:[
    CreateEditUsersComponent,
  ],
})

export class AdminLayoutModule {}
