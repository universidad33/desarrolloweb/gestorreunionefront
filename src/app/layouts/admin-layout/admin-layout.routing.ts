import { Routes } from '@angular/router';

import { HomeComponent } from '../../pages/home/home.component';
import { UsersComponent } from '../../pages/users/users.component';
import { MeetComponent } from '../../pages/monitoring/meet.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'home',           component: HomeComponent },
    { path: 'users',          component: UsersComponent },
    { path: 'monitoring',     component: MeetComponent },
];
