import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
// import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';
import { LoginComponent } from './pages/login/login.component';
import { JwtInterceptorService } from './services/interceptor/jwt-interceptor.service';
// import { CreateSvComponent } from './pages/monitoring/modal/create-sv/create-sv.component';
// import { EditSvComponent } from './pages/monitoring/modal/edit-sv/edit-sv.component';
// import { DelectSvComponent } from './pages/monitoring/modal/delect-sv/delect-sv.component';
// import { ClientDetailComponent } from './pages/monitoring/modal/client-detail/client-detail.component';
// import { VirtualServicesDetailComponent } from './pages/monitoring/modal/virtual-services-detail/virtual-services-detail.component';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormValidationsDirective } from './services/utils/validations/form-validations.directive';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import { CreateMeetComponent } from './pages/monitoring/modal/create-meet/create-meet.component';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    MatInputModule,
    MomentDateModule,
    MatSelectModule,
    MatCardModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatAutocompleteModule,
    MatIconModule,
    MatDatepickerModule,
    NgIdleKeepaliveModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    })
    
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    // CreateSvComponent,
    // EditSvComponent,
    // ClientDetailComponent,
    // DelectSvComponent,
    // VirtualServicesDetailComponent,
    FormValidationsDirective,
    CreateMeetComponent
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
