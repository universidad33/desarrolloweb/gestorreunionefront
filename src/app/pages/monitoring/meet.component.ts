import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { AlertService } from 'app/services/utils/alert.service';
import { UsersService } from 'app/services/users.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog'
// import { EditSvComponent } from './modal/edit-sv/edit-sv.component'
// import { DelectSvComponent } from './modal/delect-sv/delect-sv.component'
// import { CreateSvComponent } from './modal/create-sv/create-sv.component'
// import { VirtualServicesDetailComponent } from './modal/virtual-services-detail/virtual-services-detail.component';
import { Meet } from 'app/services/utils/models/Meet';
import { UserEdit } from 'app/services/utils/models/UserEdit';
import { UserCreate } from 'app/services/utils/models/UserCreate';
import { MeetService } from 'app/services/meet.service';
import { CreateMeetComponent } from './modal/create-meet/create-meet.component';

@Component({
  selector: 'app-meet',
  templateUrl: './meet.component.html'
})
export class MeetComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;
  ClientService: Meet;
  UserCreate: UserCreate = new UserCreate();
  UserEdit: UserEdit = new UserEdit();
  user:any = [];
  aplications = [];
  usersNameSV: string[] = [];
  aplicationsIni = [];
  AllMeets: Meet[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(
    private meetService: MeetService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    private datatablesService: DatatablesService,
    private usersService: UsersService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.user = this.usersService.getUser();
    this.buildDataTeble();
    this.getAllClient();
    this.getUsersSV();
  }

  buildDataTeble() {
    this.dtOptions = this.datatablesService.config();
  }

  addMeet(){
    const dialogRef = this.dialog.open(CreateMeetComponent, {
      disableClose: false,
      data: {

      },
      width: '900px',
      // height: '650px'
    });
  
  }

  getUsersSV(){
    // this.vitrualService.getAllUserSV().subscribe(response => {
    //   if(response.status === 200) {
    //     this.usersNameSV = response.data;
    //   }
    // });
  }

  getAllClient() {
    this.loadingDt = true;
    let clien = null;
    this.meetService.getAllMeet().subscribe((resp) => {
      this.loadingDt = false;
      this.AllMeets = resp[0].original.data;
      clien = resp.data;
    });
    return clien;
  }

  renderTable(): void {
    setTimeout(() => {
      this.aplications = this.aplicationsIni;
      this.dtTrigger.next();
    },10)
  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiene el id de la solicitud a editar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
    detailVirtualService(id) {
    // const usernamesService = this.usersNameSV;
    // const dialogRef = this.dialog.open(VirtualServicesDetailComponent, {
    //     data: {
    //         id,
    //         // usernamesService
    //     },
    //     disableClose: false,
    // });

  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiene el id de la solicitud a editar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
  editVirtualService(id, AllMeets) {
  //   const usernamesService = this.usersNameSV;
  //   const dialogRef = this.dialog.open(EditSvComponent, {
  //   data: {
  //     id,
  //     AllMeets,
  //     usernamesService
  //   },
  //   disableClose: false,
  // });

  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiente el id del servicio a eliminar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
  delect(id) {
    // const dialogRef = this.dialog.open(DelectSvComponent, {
    //   data: {
    //     id,
    //   },
    //   disableClose: false,
    // });
  }
}
