import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { fechaValidator, nameValidator, validateXSS, horaValidator } from 'app/services/utils/validations/form-validations.directive';
import { MeetService } from 'app/services/meet.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';



@Component({
  selector: 'app-create-meet',
  templateUrl: './create-meet.component.html',
})
export class CreateMeetComponent implements OnInit {
  public meetForm: FormGroup;
  control = new  FormControl();

  constructor(
    private formBuild: FormBuilder,
    private meetService: MeetService,
    private notifyService: NotifyService,
    private dialogRef: MatDialogRef<CreateMeetComponent>,
    private _router: Router,
  ) { 
  }

  ngOnInit(): void {
    this.buildForm();
  }

  get fecha() { return this.meetForm.get('fecha'); }
  get hora() { return this.meetForm.get('hora'); }
  get asunto() { return this.meetForm.get('asunto'); }
  get description() { return this.meetForm.get('description'); }

  buildForm() {
    this.meetForm = this.formBuild.group({
      fecha: new FormControl('', [
        Validators.required, Validators.maxLength(200), validateXSS()//, fechaValidator(),
      ]),
      hora: new FormControl('', [
        Validators.required, Validators.maxLength(200), validateXSS()//, horaValidator(),
      ]),
      asunto: new FormControl('', [
        Validators.required, Validators.maxLength(200), nameValidator(), validateXSS()
      ]),
      description : new FormControl('', [
        Validators.required, Validators.maxLength(100) , nameValidator(), validateXSS()
      ]),
    });
  }

  save() { 
    if(this.meetForm.valid) {
      const data = this.meetForm.value;
      this.meetService.addMeet(data).subscribe( resp => {
        this.message(resp[0].original);
      });
    }
  }

  cancelar() { 
    this.dialogRef.close();
  }

  message(resp):void {
    if (resp.status === 200) {
      this.notifyService.success(resp.mensaje);
      this.dialogRef.close();
      this._router.navigate(['/monitoring']);
    } else {
      this.notifyService.warning(resp.mensaje);
    }
  }
}
