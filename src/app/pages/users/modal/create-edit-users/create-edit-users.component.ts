import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from 'app/services/users.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { fechaValidator, nameValidator, validateXSS } from 'app/services/utils/validations/form-validations.directive';

@Component({
  selector: 'app-create-edit-users',
  templateUrl: './create-edit-users.component.html'
})

export class CreateEditUsersComponent implements OnInit {
  public clientForm: FormGroup;
  public showPasswordRep: boolean;
  roles : string;
  loadingDt: boolean = false;
  formGroup : FormGroup;
  control = new  FormControl();
  name_action = false;

  constructor(
    private formBuild: FormBuilder,
    private usersService: UsersService,
    private notifyService: NotifyService,
    private _router: Router,
    private dialogRef: MatDialogRef<CreateEditUsersComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) { }

  ngOnInit(): void {
    this.name_action = this.data.name_action;
    this.roles = this.data.roles;
    this.buildForm();
  }

  get name() { return this.clientForm.get('name'); }
  get email() { return this.clientForm.get('email'); }
  get role_id () { return this.clientForm.get('role_id'); }

  get client() {
    return this.clientForm.get('client').value;
  }

  buildForm() {
    this.clientForm = this.formBuild.group({
      name: new FormControl('', [
        Validators.required, Validators.maxLength(200), nameValidator(), validateXSS()
      ]),
      email: new FormControl('', [
        Validators.required, Validators.maxLength(200), nameValidator(), validateXSS()
      ]),
      password: new FormControl('', [
        Validators.required, Validators.maxLength(200), nameValidator(), validateXSS()
      ]),
      role_id : new FormControl('', [
        Validators.required, Validators.maxLength(100)
      ]),
    });
    if(this.data.edit) {
      this.clientForm.controls['name'].setValue(this.data.user.name);
      this.clientForm.controls['role_id'].setValue(this.data.user.role_id);
    }
}

save(): void {
  if(this.clientForm.valid){
    const data = this.clientForm.value;
    if(this.data.edit) {
      const dataEdit = {
        'id' : this.data.user.id,
        'name' : this.name.value,
        'role_id' : this.role_id.value
      };
    } else {
      this.usersService.addUser(data).subscribe(resp => {
        this.message(resp[0].original);
      });
    }

  }else{

    if(this.clientForm.value.id_client === 'invalid') {
      this.notifyService.error('Seleccione un cliente valido');

    }
    this.notifyService.warning('Ingrese todos los datos');
  }
}

  message(resp):void {
    if (resp.status === 200) {
      this.notifyService.success(resp.mensaje);
      this.dialogRef.close();
      this._router.navigate(['/users']);
    } else {
      this.notifyService.warning(resp.mensaje);
    }
  }
  cancelar(): void {
    this.dialogRef.close();
  }

}
