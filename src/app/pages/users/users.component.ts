import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatablesService } from 'app/services/utils/datatables.service';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { CreateEditUsersComponent } from './modal/create-edit-users/create-edit-users.component';
import { AlertService } from 'app/services/utils/alert.service';
import { NotifyService } from 'app/services/utils/notify.service';
import { UsersService } from 'app/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;
  users = [];

  roles:any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  columsTable = [
    { data: "id" },
    { data: "user" },
    { data: "name" },
    { data: "role.name" },
    { render: function (data: any, type: any, full: any) {
        return `<button class="btn btn-primary btn-link btn-just-icon mat-raised-button mat-button-base editTbl" mat-raised-button>
            <span class="mat-button-wrapper"><i class="material-icons">mode_edit</i></span>
          </button>
          <button class="btn btn-danger btn-link btn-just-icon mat-raised-button mat-button-base deleteTbl" mat-raised-button>
            <span class="mat-button-wrapper"><i class="material-icons">delete</i></span>
          </button>`;
      } 
    },
  ];

  constructor(private datatablesService: DatatablesService,
    private usersService: UsersService,
    private alertService: AlertService,
    private notifyService: NotifyService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getUsers();
    this.getRoles();
  }

  getUsers(){
    this.usersService.getUserAll().subscribe(resp => {
      console.log( resp[0].original.data);
      this.users = resp[0].original.data;
    });
  }

  getRoles(){
    this.usersService.getRoles().subscribe(resp => {
      this.roles =resp[0].original.data;
    });
  }

  addUser(){
    const dialogRef = this.dialog.open(CreateEditUsersComponent, {
      data: { name_action : 'Agregar', roles : this.roles },
      width: '360px',
      disableClose: true
    });

    this.closeDialog(dialogRef);
  }

  editUser(dataUser:any){
    const dialogRef = this.dialog.open(CreateEditUsersComponent, {
      data: { user: dataUser, roles: this.roles, edit: true, name_action : 'Editar'},
      width: '360px',
      disableClose: true
    });
    
    this.closeDialog(dialogRef);
  }

  closeDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ok') {
        this.renderTable();
      }
    });
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

}
